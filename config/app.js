module.exports = {
  appName: 'Bailarn',
  port: 1337,
  oauth: {
    tokenLife: 3600
  } 
};
