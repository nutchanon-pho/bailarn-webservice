/**
 * EventController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	testcreate: function(req, res) {
		var d = new Date();
		Event.create({
			course: 2,
			name: 'Exam',
			datetime_start: d,
			datetime_end: d+36000,
			type: 'Exam'
		}).exec(function(err, event) {
			if (err) console.log(err);
			else
				res.json(event);
		});
	}
};