/**
 * MajorController
 *
 * @description :: Server-side logic for managing Majors
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	findByFaculty : function(req,res){
		Major.find({faculty : req.param('id')}).populate('faculty').exec(function(err, majors){
			if(err)
			{
				return res.json(err);
			}
			return res.json(majors);
		});
	}
};

