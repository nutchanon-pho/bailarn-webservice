/**
 * User_record_voiceController
 *
 * @description :: Server-side logic for managing User_record_voices
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	recordVoice: function(req,res){
		var user = req.param('user');
		var material = req.param('material');

		User_record_voice.create({
			user : user,
			material : material
		}).exec(function(err){
			if(err)
				res.send(403);
			else
				res.send(200);
		});
	}
};

