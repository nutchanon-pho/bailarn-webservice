/**
 * Dictionary_historyController
 *
 * @description :: Server-side logic for managing dictionary_histories
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	addHistory: function(req, res) {
		var access_token = req.param('access_token');
		var keyword = req.param('keyword');

		AccessToken.findOne({
			token: access_token
		}).exec(function(err, token) {
			if (err) {
				return res.send(err);
			} else if (!token) {
				return res.send(404);
			} else {
				Dictionary_history.findOne({
					user: token.userId,
					keyword: keyword
				}).exec(function(err, history) {
					if (err) {
						return res.send(err);
					} else if (!history) {
						Dictionary_history.create({
							keyword: keyword,
							count: 1,
							user: token.userId
						}).exec(function(err) {
							if (err) {
								return res.send(err);
							} else {
								return res.send(200);
							}
						});
					} else {
						history.count++;
						history.save(function(err) {
							if (err) {
								return res.send(err);
							} else {
								return res.send(200);
							}
						});
					}
				});
			}
		});
	}
};