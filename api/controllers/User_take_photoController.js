/**
 * User_take_photoController
 *
 * @description :: Server-side logic for managing User_take_photoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	takePhoto: function(req,res){
		var user = req.param('user');
		var material = req.param('material');

		User_take_photo.create({
			user : user,
			material : material
		}).exec(function(err){
			if(err)
				res.send(403);
			else
				res.send(200);
		});
	}
};

