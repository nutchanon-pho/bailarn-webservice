/**
 * ClassController
 *
 * @description :: Server-side logic for managing Classes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	todayClass: function(req, res) {
		var access_token = req.param('access_token');
		AccessToken.findOne({
			token: access_token
		}).exec(function(err, token) {
			User.findOne(token.userId).populateAll().exec(function(err, user) {
				var enrolledCourses = [];
				for (var i = 0; i < user.courses.length; i++) {
					enrolledCourses.push(user.courses[i].id);
				}
				var todayStart = new Date();
				todayStart.setHours(0);
				todayStart.setMinutes(0);
				todayStart.setSeconds(0);
				var todayEnd = new Date();
				todayEnd.setHours(23);
				todayEnd.setMinutes(59);
				todayEnd.setSeconds(59);
				Class.find({
					datetime_start: {
						'>': todayStart,
						'<': todayEnd
					},
					course: enrolledCourses,
					status : ['Normal','Makeup']
				}).populateAll().exec(function(err, c) {
					res.json(c);
				});
			});
		});

	},
	getMaterials: function(req, res) {
		var class_id = req.param('class_id');
		Material.find({
			class: class_id
		}).populateAll().exec(function(err, m) {
			res.json(m);
		});
	}
};