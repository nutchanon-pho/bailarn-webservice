/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	'all': function(req, res) {
		User.find().populateAll().exec(function(err, users) {
			res.json(users);
		});
	},
	'findByUserId': function(req, res) {
		var userId = req.param('id');
		User.findOne({
			user_id: userId
		})
			.populate('university')
			.populate('faculty')
			.exec(function(err, user) {
				if (err) {
					res.json(err);
				} else if (!user) {
					res.send('User not found.', 404);
				} else {
					res.json(user);
				}
			});
	},
	'new': function(req, res) {
		University.find().populate('faculties').exec(function(err, universities) {
			if (err)
				res.json(err)
			else {
				res.view({
					universities: universities
				});
			}
		});
	},
	create: function(req, res) {
		User.create({
			user_id: req.param('student_id'),
			firstname: req.param('firstname'),
			lastname: req.param('lastname'),
			gender: req.param('gender'),
			year: req.param('year'),
			section: req.param('section'),
			major: req.param('student_id'),
			username: req.param('username'),
			password: req.param('password'),
			email: req.param('email'),
			tel: req.param('tel'),
			birthday: req.param('birthday'),
			type: 'Student',
			university: req.param('university'),
			faculty: req.param('faculty')
		}).exec(function(err, user) {
			if (err) {
				res.json(err);
			} else {
				res.json(user);
			}
		});
	},
	createInstructor: function(req, res) {
		User.create({
			user_id: req.param('user_id'),
			firstname: req.param('firstname'),
			lastname: req.param('lastname'),
			gender: req.param('gender'),
			year: req.param('year'),
			section: req.param('section'),
			major: req.param('student_id'),
			username: req.param('username'),
			password: req.param('password'),
			email: req.param('email'),
			tel: req.param('tel'),
			birthday: req.param('birthday'),
			type: 'Instructor',
			university: req.param('university'),
			faculty: req.param('faculty')
		}).exec(function(err, user) {
			if (err) {
				res.json(err);
			} else {
				res.json(user);
			}
		});
	},
	'index': function(req, res) {
		User.find({type : 'Student'})
			.populate('university')
			.populate('faculty')
			.populate('major')
			.exec(function(err, users) {
				if (err) {
					return res.json(err);
				} else {
					res.view('user/index', {
						users: users
					});
				}
			});
	},
	login: function(req, res) {
		res.view();
	},
	getCurrentUser: function(req, res) {
		var access_token = req.param('access_token');
		AccessToken.findOne({
			token: access_token
		}).exec(function(err, token) {
			if (err || !token) res.json(err);
			else {
				User.findOne(token.userId).populateAll().exec(function(err, user) {
					res.json(user);
				});
			}
		});
	}
};