/**
 * CourseController
 *
 * @description :: Server-side logic for managing Courses
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {
	// @param User's ObjectId
	// @return Courses that the user enrolled
	'getCourses': function(req, res) { //53ba20dae88af3d515a2e2ca
		var access_token = req.param('access_token');
		AccessToken.findOne({
			token: access_token
		}).exec(function(err, token) {
			if (err) res.json(err);
			else {
				User.findOne(token.userId)
					.populateAll()
					.exec(function(err, user) {
						if (err) {
							res.json(err);
						} else {
							var enrolledCourses = [];
							for (var i = 0; i < user.study_courses.length; i++) {
								enrolledCourses.push(user.study_courses[i].id);
							}
							Course.find(enrolledCourses).populateAll().exec(function(err, c) {
								res.json(c);
							});
						}
					});
			}
		});
	},
	getMaterials: function(req, res) {
		var courseId = req.param('course_id');
		Material.find({
			course: courseId
		}).populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Material not found.');
			else
				res.json(m);
		});
	},
	index: function(req, res) {
		User.findOne(req.user.id).populateAll().exec(function(err, user) {
			var enrolledCourses = [];
			for (var i = 0; i < user.teach_courses.length; i++) {
				enrolledCourses.push(user.teach_courses[i].id);
			}
			Course.find(enrolledCourses).populateAll().exec(function(err, courses) {
				var students = [];
				for (var i = 0; i < courses.length; i++) {
					var course = courses[i];
					var studentsInThisCourse = [];
					for (var j = 0; j < course.study_by.length; j++) {
						var student = course.study_by[j];
						studentsInThisCourse.push(student);
					}
					students.push(studentsInThisCourse);
				}
				res.view({
					courses: courses,
					students: students
				});
			});
		});
	},
	getStudents: function(req, res) {
		var courseId = req.param('id');
		Course.findOne(courseId).populate('study_by').exec(function(err, course) {
			if (err || !course) {
				return res.send(404, 'Course not found');
			} else {
				return res.json(course.study_by);
			}
		});
	},
	withdraw: function(req, res) {
		var studentId = req.param('id');
		var courseId = req.param('course');
		Course.findOne(courseId).exec(function(err, course) {
			if (err) {
				return res.send(err);
			} else {
				course.study_by.remove(studentId);
				course.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},
	add: function(req, res) {
		var studentId = req.param('id');
		var courseId = req.param('course');
		var universityId = req.user.university;
		var facultyId = req.user.faculty;
		console.log(studentId + ' ' + courseId + ' ' + universityId + ' ' + facultyId);
		Course.findOne(courseId).exec(function(err, course) {
			if (err || !course) {
				return res.send('Course not found.');
			} else {
				User.findOne({
					user_id: studentId,
					university: universityId,
					faculty: facultyId
				}).exec(function(err, student) {
					if (err || !student) {
						res.send('Student not found')
					} else {
						course.study_by.add(student);
						course.save(function(err) {
							if (err) {
								res.send('Student is already enrolled');
							} else {
								res.send('Student has been enrolled.');
							}
						});
					}
				});
			}
		});
	},
	upload: function(req, res) {
		University.findOne(req.user.university).exec(function(err, university) {
			Faculty.findOne(req.user.faculty).exec(function(err, faculty) {
				Course.findOne(req.param('course')).exec(function(err, course) {
					var d = new Date();
					var timestamp = d.getTime();
					var sessionID = req.sessionID;
					var uniqueFilename = timestamp + sessionID + '-';
					var directory = '/assets/course/';

					var receiver = blobAdapter().receive({
						dirname: sails.config.appPath + directory,
						saveAs: function(file) {
							var filename = file.filename,
								newName = uniqueFilename + filename;
							return newName;
						}
					});

					mkdirp(directory, function(err) {
						req.file('student_list')
							.upload(receiver, function onUploadComplete(err, uploadedFiles) {
								if (err) return res.send(err);
								else {
									try {
										var file = uploadedFiles[0];
										if (typeof require !== 'undefined') XLS = require('xlsjs');
										//Read file
										var workbook = XLS.readFile(sails.config.appPath + directory + uniqueFilename + file.filename);
										//Delete file
										fs.unlink(sails.config.appPath + directory + uniqueFilename + file.filename, function(err) {
											if (err) {
												return res.send(err);
											}

										});
										var sheetNames = workbook.SheetNames;
										var students = [];

										for (var i = 0; i < sheetNames.length; i++) {
											var sheetName = sheetNames[i];
											var sheet = workbook.Sheets[sheetName];
											var cellnumber;
											if (sheet.B8.v) { //Small Version students' ids start at B6
												cellnumber = 8;
											} else if (sheet.B6.v) { //Full Version students' ids start at B8
												cellnumber = 6;
											}
											try {
												do {
													var studentIdCell = 'B' + cellnumber;
													var studentId = sheet[studentIdCell].w;
													students.push(studentId);
													cellnumber++;
												}
												while (true);
											} catch (err) {
												console.log(err);
											}
											//sections.push(students);
										}
									} catch (err) {
										res.send(err);
									}
									User.find({
										university: university.id,
										faculty: faculty.id,
										user_id: students
									}).exec(function(err, users) {
										users.forEach(function(user) {
											user.study_courses.add(course);
											user.save(function(err) {
												if (err) {
													console.log(err);
												}
											});
										});
									});
									res.send(200);
								}
							});
					});
				});
			});
		});
	}
};