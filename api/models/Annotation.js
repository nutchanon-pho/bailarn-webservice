/**
 * Assignment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		creator : {
			model : 'User',
			required : true
		},
		material : {
			model : 'Material',
			required : true
		},
		items : {
			collection : 'AnnotationItem',
			via : 'annotation'
		}
	}
};