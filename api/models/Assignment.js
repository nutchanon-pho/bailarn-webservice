/**
 * Assignment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		course: {
			model: 'Course',
			required: true
		},
		title: {
			type: 'string',
			required: true
		},
		description: {
			type: 'text'
		},
		submit_datetime: {
			type: 'datetime',
			required: true
		}
	}
};