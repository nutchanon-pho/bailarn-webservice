/**
 * Faculty.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		name: {
			type: 'string',
			required: true
		},
		university: {
			model: 'University',
			required: true
		},
		tel: {
			type: 'int'
		},
		address: {
			type: 'text'
		},
		server_path: {
			type: 'string'
		},
		majors: {
			collection: 'Major',
			via: 'faculty'
		}
	}
};