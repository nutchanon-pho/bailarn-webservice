/**
 * Class.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		course: {
			model: 'Course',
			required: true
		},
		datetime_start: {
			type: 'datetime',
			required: true
		},
		datetime_end: {
			type: 'datetime',
			required: true
		},
		location: {
			type: 'string',
			required: true
		},
		section: {
			type: 'int'
		},
		status: {
			type: 'string',
			required: true
		}, //Makeup Cancel Normal
		lecture_flag: {
			type: 'boolean'
		},
		lab_flag: {
			type: 'boolean'
		},
		quiz_flag: {
			type: 'boolean'
		}
	}
};