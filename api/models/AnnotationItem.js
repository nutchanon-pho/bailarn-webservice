/**
 * AnnotationItem.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		page: {
			type: 'int',
			required: true
		},
		type: { //(audio/img/text/mark)
			type: 'string',
			required: true
		},
		text: 'text',
		filepath: 'string',
		annotation: {
			model: 'Annotation',
			required: true
		}
	}
};