/**
 * Course.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		university: {
			model: 'University',
			required: true
		},
		faculty: {
			model: 'Faculty',
			required: true
		},
		course_code: {
			type: 'string',
			required: true
		},
		name: {
			type: 'string',
			required: true
		},
		credit: {
			type: 'int',
			required: true
		},
		description: {
			type: 'text'
		},
		semester: {
			type: 'int',
			required: true
		},
		year: {
			type: 'int',
			required: true
		},
		todos: {
			collection: 'Todo',
			via: 'course'
		},
		prerequisites: {
			collection: 'Course'
		},
		teach_by: {
			collection: 'User',
			via : 'teach_courses',
			dominant : true
		},
		study_by: {
			collection: 'User',
			via : 'study_courses',
			dominant: true
		},
		classes: {
			collection : 'Class',
			via : 'course'
		}
	}
};