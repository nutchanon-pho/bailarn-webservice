$(document).ready(function() {
	$('#university').on('load change', function() {
		var optionSelected = $("option:selected", this);
		$.ajax({
			type: 'get',
			url: '/university/' + optionSelected.val()
		}).success(function(res) {
			console.log(res);
			$('#faculty').html('');
			res.faculties.forEach(function(entry) {
				$('#faculty').append('<option value="' + entry.id + '">' + entry.name + '</option>');
			});
		});
	});
});