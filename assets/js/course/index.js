$(document).ready(function() {
	function setupDeleteButton(){
		$('button.btn.btn-danger.deleteButton').click(function() {
				var button = $(this);
				var studentName = button.prop('name');
				var studentId = button.prop('value');
				var courseName = $('li.active').children().html();
				var courseId = $('li.active').prop('id');
				bootbox.confirm('Are you sure you want to withdraw ' + studentName + ' from ' + courseName + '?', function(result) {
					if (result) {
						button.addClass("disabled");
						$.ajax({
							type: 'delete',
							url: '/course/withdraw?id=' + studentId + '&course=' + courseId
						}).done(function(response) {
							console.log('Finish');
							console.log(response);
							$('tr[id=' + studentId + ']').css("color", "lightgrey");
							getStudents(courseId);
						});
					}
				});

			});
	}

	function getStudents(courseId) {
		$.ajax({
			type: 'get',
			url: '/course/getStudents?id=' + courseId
		}).done(function(response) {
			console.log('s');
			var table = $('div.active').children('table').children('tbody');
			table.html('');
			for (var i = 0; i < response.length; i++) {
				var name = response[i].firstname + ' ' + response[i].lastname;
				var $tr = $('<tr>');
				var content = '<td>' + (i + 1) + '</td>';
				content += '<td>' + response[i].user_id + '</td>';
				content += '<td>' + name + '</td>';
				content += '<td>' + response[i].section + '</td>';
				content += '<td><button class="btn btn-danger deleteButton" value="' + response[i].id + '" name="' + name + '">Delete</button></td>';
				$tr.append(content);
				table.append($tr);
			}
			setupDeleteButton();
		});
	}
	setupDeleteButton();
	$('ul.nav.nav-tabs').children().children('a').click(function(){
		var courseId = $(this).parent().prop('id');
		getStudents(courseId);
	});

	$('#refreshButton').click(function(){
		var courseId = $('li.active').prop('id');
		getStudents(courseId);
	});

	$('.addStudentForm').submit(function(e) {
		var courseId = $(this).children().children('[name=course]').prop('value')
		var data = new FormData(this);
		var options = {
			url: '/course/add',
			type: 'POST',
			success: function(responseText, statusText, xhr, $form) {
				$('#uploadResultModal div.modal-body').html(responseText);
				$('#uploadResultModal').modal('show');
				getStudents(courseId);
			}
		};
		$(this).ajaxSubmit(options);
		$(this).clearForm();
		e.preventDefault();
	});

	$('.uploadForm')
		.submit(function(e) {
			var courseId = $(this).children().children('[name=course]').prop('value');
			console.log(courseId);
			var data = new FormData(this);
			var progress = $(this).children().children('.progress');
			var progressBar = $(this).children().children().children('.progress-bar')
			var cancelButton = $(this).children().children().children('button');
			var options = {
				url: '/course/upload',
				type: 'POST',
				beforeSubmit: function() {
					progressBar.css('width', '0%');
					progress.show();
					cancelButton.show();
				},
				uploadProgress: function(event, position, total, percentComplete) {
					if (percentComplete == 100) {
						progressBar.css('width', percentComplete + '%').html('Processing...');
						progress.hide('slow');
						cancelButton.hide();
					} else {
						progressBar.css('width', percentComplete + '%').html(percentComplete + '%');
					}
				},
				beforeSend: function(xhr) {
					cancelButton.click(xhr.abort);
				},
				success: function(responseText, statusText, xhr, $form) {
					$('#uploadResultModal div.modal-body').html("Students has been enrolled.");
					$('#uploadResultModal').modal('show');
					getStudents(courseId);
				}
			}
			$(this).ajaxSubmit(options);
			$(this).clearForm();
			e.preventDefault();
		});

});