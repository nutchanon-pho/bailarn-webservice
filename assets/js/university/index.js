function getMajors(id, faculty) {
	$.ajax({
		type: 'get',
		url: '/major/findByFaculty/' + id
	}).success(function(majors) {
		if (isNotNull(majors)) {
			console.log(majors);

			appendMajors(faculty, majors);
		}

	});
}

function appendMajors(faculty, majors) {
	majors.forEach(function(major){
		faculty.append('<li>'+ major.name +'</li>');
	});
}

function isNotNull(object){
	return object.length>0;
}

$(".faculty").each(function(index) {
	var facultyID = $(this).attr('id');
	getMajors(facultyID, $(this));
});